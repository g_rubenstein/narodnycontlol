json.array!(@claims) do |claim|
  json.extract! claim, :originator_id, :title, :description, :category_id, :address, :pos_lat, :pos_lon, :pos_alt, :created_at, :registered_at, :registration_no, :status, :status_code, :deadline, :rate, :authority_response, :authority_response_date
  json.url claim_url(claim, format: :json)
end
