json.array!(@users) do |user|
  json.extract! user, :first_name, :patronymic_name, :last_name, :login, :email, :pass_hash, :address, :phone
  json.url user_url(user, format: :json)
end
