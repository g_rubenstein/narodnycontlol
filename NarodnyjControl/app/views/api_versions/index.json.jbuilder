json.array!(@api_versions) do |api_version|
  json.extract! api_version, :version, :uri, :description
  json.url api_version_url(api_version, format: :json)
end
