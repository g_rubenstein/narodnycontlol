json.array!(@claim_classifiers) do |claim_classifier|
  json.extract! claim_classifier, :name, :description, :begin_date, :end_date
  json.url claim_classifier_url(claim_classifier, format: :json)
end
