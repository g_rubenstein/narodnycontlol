json.array!(@sessions) do |session|
  json.extract! session, :sid, :isid, :access_time, :user_id
  json.url session_url(session, format: :json)
end
