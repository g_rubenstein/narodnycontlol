require 'digest/sha2'

class User < ActiveRecord::Base
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :login, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true
  validates :phone, :presence => true, :uniqueness => true

  validates :password, :confirmation => true
  attr_accessor :password_confirmation
  attr_reader   :password
  validate  :password_must_be_present
  serialize :settings, Hash
  
  def User.authenticate(login, password_hash, isid)
    if user = find_by_login(login)
      if password_hash == encrypt_password_hash(user.pass_hash, isid)
        user
      end
    end
  end
  
  def User.encrypt_password_hash(password_hash, salt)
    Digest::SHA2.hexdigest(password_hash + salt)
  end
  
  def password=(password)
    @password = password

    if password.present?
      self.pass_hash = self.class.encrypt_password_hash(password, self.login)
    end
  end
      
  private
  def password_must_be_present
    raiss "empty password" unless pass_hash.present?
  end
end
