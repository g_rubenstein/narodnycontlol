class ClaimClassifiersController < ApplicationController
  before_action :set_claim_classifier, only: [:show, :edit, :update, :destroy]

  # GET /claim_classifiers
  # GET /claim_classifiers.json
  def index
    @claim_classifiers = ClaimClassifier.all
  end

  # GET /claim_classifiers/1
  # GET /claim_classifiers/1.json
  def show
  end

  # GET /claim_classifiers/new
  def new
    @claim_classifier = ClaimClassifier.new(:parent_id => params[:parent_id])
    @claim_classifier["end_date"] = Time.now + 60*60*24*365
  end

  # GET /claim_classifiers/1/edit
  def edit
  end

  # POST /claim_classifiers
  # POST /claim_classifiers.json
  def create
    @claim_classifier = ClaimClassifier.new(claim_classifier_params)

    respond_to do |format|
      if @claim_classifier.save
        format.html { redirect_to @claim_classifier, notice: 'Claim classifier was successfully created.' }
        format.json { render action: 'show', status: :created, location: @claim_classifier }
      else
        format.html { render action: 'new' }
        format.json { render json: @claim_classifier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /claim_classifiers/1
  # PATCH/PUT /claim_classifiers/1.json
  def update
    respond_to do |format|
      if @claim_classifier.update(claim_classifier_params)
        format.html { redirect_to @claim_classifier, notice: 'Claim classifier was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @claim_classifier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /claim_classifiers/1
  # DELETE /claim_classifiers/1.json
  def destroy
    @claim_classifier.destroy
    respond_to do |format|
      format.html { redirect_to claim_classifiers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_claim_classifier
      @claim_classifier = ClaimClassifier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def claim_classifier_params
      params.require(:claim_classifier).permit(:name, :description, :begin_date, :end_date, :parent_id)
    end
end
