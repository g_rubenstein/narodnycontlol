require 'error_helper'
class Api::ApplicationController < ActionController::Base
  include ErrorHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :session_expiration_check
  protect_from_forgery with: :null_session
  
  private
  
  def session_expiration_check
    return render_no_session_login_error unless valid_user?
  end
end
