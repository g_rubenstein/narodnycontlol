require 'error_helper'
require "uuidtools"

class Api::AuthController < Api::ApplicationController
  include ErrorHelper
  # Skip session check
  skip_before_filter :session_expiration_check, :except => [:update]
  
  def login
    isid = params[:isid]
    
    # Check ISID
    begin
      isid = UUIDTools::UUID.parse(isid).to_s
    rescue Exception
      render_error 'show', 'wrong.isid'
      return
    end
    
    # Get API version record
    verRecord = ApiVersion.where(["version=?", 'v1']).select("uri").first
    
    # Check if we support this api version
    return render_error 'show', 'unsupported.api.version' unless !verRecord.nil?
    
    # Generate new session ID
    newSessionGUID = SecureRandom.uuid
    
    # Store new session
    @session = Session.create(:sid=>newSessionGUID, :isid => isid, :access_time => Time.now,
      :user_id => -1)
   
    if user = User.authenticate(params[:uid], params[:pass], @session.isid)
      @session.user_id = user.id
      @session.save
    else
      render_error('show', 'auth.wrong_credentials')
    end

    baseURI = verRecord.uri % {:SID => newSessionGUID}
    reply = {
      :success => true,
      :mostRecentClassifierVersion => ClaimClassifier.maximum(:updated_at),
      :mostRecentClaimUpdate => Claim.maximum(:updated_at),
      :baseURI => baseURI,
      :SID => newSessionGUID,
      :timeout => SETTINGS['session_timeout_seconds'],
      :citizen => user.attributes.select {|k,v| [:first_name, :patronymic_name, :last_name, :login, :email, :address, :phone].include?(k.to_sym) },
      :settings => user.settings
    }
    
    
    render :json => reply
  end

  def logout
    begin
      Session.destroy_all(["sid=? and isid=?", params[:sid], params[:isid]])
    rescue Exception => e
      logger.error(e)
    end
    render :json => {:success => true}
  end

  def resetPassword
    userIndentity = params[:identity]
    user = User.where("login = :userIndentity or email = :userIndentity or phone = :userIndentity", :userIndentity => userIndentity ).first
    return render_error('show', 'auth.user_not_found') unless !user.nil?
    
    #send password reset link
    #send_password_reset_link(user)
    render :json => {:success => true}
  
  end

  def update
    user = User.find_by_id(@session.user_id)
    return render_error('show', 'auth.no_such_user') unless !user.nil?
    return render_error('show', 'auth.old_pass_mismatch') unless user.pass_hash == params[:prev_pass_hash]
    begin
      User.update(:login=> params[:login], :email => params[:email], :phone => params[:phone],
        :pass_hash => params[:pass_hash], :address => params[:address], :first_name => params[:first_name], :patronymic_name => params[:patronymic_name],
        :last_name => params[:last_name])
      render :json => {:success => true}
    rescue Exception
      render_error('show', 'auth.user.update_failed')
    end
  end
  
  def update_settings
    user = User.find_by_id(@session.user_id)
    return render_error('show', 'auth.no_such_user') unless !user.nil?
    begin
      User.update(:settings => params[:settings])
      render :json => {:success => true}
    rescue Exception
      render_error('show', 'auth.user_settings.update_failed')
    end
  end


  def register
    #Check for duplicates
    user = User.where("login = :login or email = :email or phone = :phone", 
      :login => params[:login], :email => params[:email], :phone => params[:phone] ).first

    return render_error('show', 'auth.user_exists') unless user.nil?
    
    begin
      user = User.create(:login => params[:login], :email => params[:email], :phone => params[:phone],
        :pass_hash => params[:pass_hash], :address => params[:address], :first_name => params[:first_name], :patronymic_name => params[:patronymic_name],
	:last_name => params[:last_name])

        render :json => {:success => true}
    rescue Exception
      render_error('show', 'auth.user.registration_failed')
    end
  end

end
