require 'json'
class Api::ClaimsController < Api::ApplicationController
  def create
    begin
      Rails.logger.debug params.inspect
      claim_object = JSON.parse(params[:claim])
      claim = Claim.new(claim_params(claim_object))
      claim.originator_id = @session.user_id
      claim.save
      render :json => claim_attributes_to_hash(claim)
    rescue Exception
      render_error('show', 'claim.createError')
    end
  end
  
  def update
    begin
      claim_object = JSON.parse(params[:claim])
      claim.update(claim_params(claim_object))
      render :json => claim_attributes_to_hash(claim)
    rescue Exception
      render_error('show', 'claim.createError')
    end
  end
  
   
  def index
    if (params[:date].nil?)
      claims = Claim.where(['originator_id = ?', @session.user_id]).order('created_at')
    else
      claims = Claim.where(['originator_id = ? and updated_at > ?', @session.user_id, params[:date].gsub('T', ' ')]).order('created_at')
    end
    render :json => claims.map { |c| claim_attributes_to_hash(c) }
  end
  
  private
    def claim_attributes_to_hash(c)
      claim = c.attributes.except('originator_id','creator_id','responsible_id','departament_id', 'registration_no')
      claim['claim_classifier'] = c.claim_classifier.name
      claim['status'] = '[text status here]' #todo:
      claim['attachments'] = [ #todo
        {
          :id => 1,
          :kind => 0,
          :preview => 'attachments/' + c.id.to_s + '/0/preview',
          :original => 'attachments/' + c.id.to_s + '/0/original',
        }
      ]
      return claim    
    end

    def claim_params(claim_object)
      claimHash = claim_object.to_hash
      permitted = ['id', 'title', 'description', 'address', 
        'pos_lat', 'pos_lon', 'pos_alt', 'created_at', 'user_rate', 'user_feedback', 'claim_classifier_id']
      return claimHash.select {|k,v|  permitted.include?(k) }
      end  
  
end