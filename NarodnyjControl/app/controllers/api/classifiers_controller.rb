class Api::ClassifiersController < Api::ApplicationController
  #skip_before_filter :session_expiration_check

  def get
    lastUpdated = DateTime.parse(params[:date]) unless params[:date].nil?
    roots = ClaimClassifier.roots
    
    return render :json => {} unless !roots.nil?

    result = Array.new
    roots.map { |root| result.push(to_tree(root, lastUpdated)) }

    render :json => result
  end
  
  private
    def to_tree(node, lastUpdated)

    if (lastUpdated.nil? or node.updated_at > lastUpdated)
      object = { 
	:id => node.id,
	:name => node.name,
        :description => node.description,
        :updated_at => node.updated_at,
	:end_date => node.end_date
      }
    else
      return nil unless node.has_children?
      object = { 
	:id => node.id
      }
    end

    children = node.children.map { |c| to_tree(c, lastUpdated) }.compact

    if (!children.nil? and children.any?) 
	object["children"] = children 
    end

    return object
  end

end
