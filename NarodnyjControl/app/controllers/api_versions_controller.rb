class ApiVersionsController < ApplicationController
  before_action :set_api_version, only: [:show, :edit, :update, :destroy]

  # GET /api_versions
  # GET /api_versions.json
  def index
    @api_versions = ApiVersion.all
  end

  # GET /api_versions/1
  # GET /api_versions/1.json
  def show
  end

  # GET /api_versions/new
  def new
    @api_version = ApiVersion.new
  end

  # GET /api_versions/1/edit
  def edit
  end

  # POST /api_versions
  # POST /api_versions.json
  def create
    @api_version = ApiVersion.new(api_version_params)

    respond_to do |format|
      if @api_version.save
        format.html { redirect_to @api_version, notice: 'Api version was successfully created.' }
        format.json { render action: 'show', status: :created, location: @api_version }
      else
        format.html { render action: 'new' }
        format.json { render json: @api_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /api_versions/1
  # PATCH/PUT /api_versions/1.json
  def update
    respond_to do |format|
      if @api_version.update(api_version_params)
        format.html { redirect_to @api_version, notice: 'Api version was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @api_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api_versions/1
  # DELETE /api_versions/1.json
  def destroy
    @api_version.destroy
    respond_to do |format|
      format.html { redirect_to api_versions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_version
      @api_version = ApiVersion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def api_version_params
      params.require(:api_version).permit(:version, :uri, :description)
    end
end
