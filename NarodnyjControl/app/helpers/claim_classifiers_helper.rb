module ClaimClassifiersHelper
  def nested_claims(claims)
    claims.map do |claim, sub_claims|
      render(claim) + content_tag(:div, nested_claims(sub_claims), :class => "nested_claim")
    end.join.html_safe
  end
end
