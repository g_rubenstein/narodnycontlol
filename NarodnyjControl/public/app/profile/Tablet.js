Ext.define('NarodnyjControl.profile.Tablet', {
    extend: 'Ext.app.Profile',

    config: {
        name: 'tablet',
        namespace: 'tablet',
        views: ['Login','Main']
    },

    isActive: function() {
        return !Ext.os.is.Phone;
    },

    launch: function() {
        //NarodnyjControl.util.Proxy.process('data/feed.js', function() {
	Ext.Viewport.add(Ext.create('NarodnyjControl.view.tablet.Login'));
        //});
    }
});
