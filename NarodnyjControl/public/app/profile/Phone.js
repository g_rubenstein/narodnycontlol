Ext.define('NarodnyjControl.profile.Phone', {
    extend: 'Ext.app.Profile',

    config: {
        name: 'phone',
        namespace: 'phone',
        views: ['Login','Main']
    },

    isActive: function() {
        return Ext.os.is.Phone;
    },
     
    launch: function() {
	Ext.Viewport.add(Ext.create('NarodnyjControl.view.phone.Login'));
        //NarodnyjControl.util.Proxy.process('data/feed.js', function() {
        //});
    }
});
