Ext.define('NarodnyjControl.util.Proxy', {

	singleton: true,

	requires: ['Ext.data.proxy.JsonP'],

	process: function(url, callback) {

		var sessionStore = Ext.getStore('Claims'),
		    sessionIds, proposalModel, speakerModel, speakerSessions = {}, sessionId, speaker, sessionDays = {};

		Ext.data.JsonP.request({
		    url: url,
		    callbackName: 'feedCb',

		    success: function(data) {

		        Ext.Array.each(data, function(proposal) {

		            proposal.speakerIds = [];
		            proposalModel = Ext.create('NarodnyjControl.model.Claim', proposal);
		            sessionStore.add(proposalModel);
		        });
		        callback();
		    }
		});

	}
});
