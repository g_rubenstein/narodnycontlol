Ext.define('NarodnyjControl.controller.Login', {
    extend: 'Ext.app.Controller',

   requires : [
   //'NarodnyjControl.view.Main',
   //'NarodnyjControl.controller.Main',
   ],

    config: {
        routes: {
            'login': 'showLogin'
        },

	refs: {
            loginButton: '#loginButton',
			loginForm: '#loginform',
			loginpanel: '#loginpanel',
			credentialsWarningPanel: '#credentialsWarningPanel'
        },

        control: {
            loginButton: {
                tap: 'doLogin'
            },
			loginpanel: {
				show: 'doShow'
			}
        }
    },

	showLogin: function()
	{
		// Logout if was logged in
	},
	
	doShow: function()
	{
		//
	},
	
	// Send App version and ISID to server and get session ID or error
	getNewSession : function(sessionUrl, appVersion, isid)
	{
		var form = this.getLoginForm();
		form.getParent().setMasked({
			xtype: 'loadmask',
			message: 'Get Session ID...'
		});
		
		var _this = this;
		var values = form.getValues();
		var login = values['login'];
		// This is database storage form
		var passHash = hex_sha256(values['password'] + login);
		// This is wire form. Server will do the same and check equality
		var hashWithIsid = hex_sha256(passHash + isid);

		Ext.Ajax.request({
			method: 'POST',
			url: sessionUrl,
			params : {
				uid : login,
				pass: hashWithIsid,
				isid: isid
			},

			success: function(response){
				form.getParent().unmask();

				alert(response.responseText);
				var tokenObject = Ext.util.JSON.decode(response.responseText);
				
				if (tokenObject.error)
				{
					Ext.Msg.alert('Get session failed', App._t(tokenObject.error.message) + ' \nAction : ' + tokenObject.error.action, Ext.emptyFn);
					return;
				}
				
				App.sid = tokenObject.SID;
				App.apiUri = tokenObject.baseURI;
				_this.loginSuccess();
			},

			failure: function(response){
				form.getParent().unmask();
				Ext.Msg.alert(response.statusText + ' #' + response.status, 'An error occured during session request' , Ext.emptyFn);
			}
			
		});	
	},
    
    
    loginSuccess : function()
    {
		App.showView('Main');
    		
    },
    
    doLogin: function() 
	{
		App.isid = uuid.v4();
		this.getNewSession(App.sessionUri, App.appVersion, App.isid);
	},
	
	checkEnableGPG: function()
	{
		var gpsEnabled = BpolApp.getInstance().queryLocalValue('enableGPSTracking', 0) == 1;
		
		if (!gpsEnabled)
		{
			return;
		}
		
		BpolApp.getInstance().locatorService().startLocation();
		BpolApp.getInstance().locatorService().stopLocation();
	}
});