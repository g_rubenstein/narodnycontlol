Ext.define('NarodnyjControl.controller.Claims', {
	extend: 'Ext.app.Controller',

	config: {
		refs: {
			claims: 'claims',
			newClaim : 'claimContainer #newButton',
			saveClaim : 'claimContainer #saveButton',
			claimContainer : 'claimContainer'
		},
		control: {
			claims: {
				initialize: 'initClaims',
				itemtap: 'onClaimTap'
			},
			newClaim: {
				tap: 'onNewClaim'
			},
			saveClaim : {
				tap: 'onSaveClaim'
			}
		}
	},

	initClaims: function() {
		var sessionStore = Ext.getStore('Claims');
		var proxy = sessionStore.getProxy();
		proxy.setUrl(App.apiUri + 'claims');

		var claimsList = this.getClaims();
		claimsList.setMasked({
			xtype: 'loadmask',
			message: 'Logging in...'
		});
		sessionStore.load({
    			callback: function(records, operation, success) {
				claimsList.unmask();
    			},
    			scope: this
		});
	},
	onNewClaim: function() {
		if (!this.claimWidget) {
			this.claimWidget = Ext.widget('claim');
		}

		this.claimWidget.setTitle("Новая заявка");
		this.getClaimContainer().push(this.claimWidget);
	},

	onClaimTap: function(list, idx, el, record) {
		if (!this.claimWidget) {
			this.claimWidget = Ext.widget('claim');
		}

		this.getNewClaim().setHidden(true);
		this.getSaveClaim().setHidden(false);
		this.claimWidget.setTitle("заявка");
		this.claimWidget.setValues(record.getData());
		this.getClaimContainer().push(this.claimWidget);
		
		Ext.get('fileUpload1').dom.click();
		
		//var speakerStore = Ext.getStore('SessionSpeakers'),
		//	speakerIds = record.get('speakerIds');

		//speakerStore.clearFilter(true);
		//speakerStore.filterBy(function(speaker) {
		//	return Ext.Array.contains(speakerIds, speaker.get('id'));
		//});

		//if (!this.session) {
		//	this.session = Ext.widget('session');
		//}

		//this.session.setTitle(record.get('title'));
		//this.getSessionContainer().push(this.session);
		//this.getSessionInfo().setRecord(record);
	},
	onSaveClaim : function()
	{
		var frm = Ext.get('uploadForm').dom;
		var claimField = Ext.get('claimJson').dom;
		claimField.value = Ext.JSON.encode(this.claimWidget.getValues());
		frm.submit();
	}
});
