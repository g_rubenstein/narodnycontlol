Ext.define('NarodnyjControl.model.Claim', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			'id',
			'title',
			{
				name: 'created_at',
				type: 'date',
				convert: function(value, record) {
					if (value) {
						var dateArr = value.split(/[\-T:]/);
						//alert(dateArr[0] + ',' + dateArr[1] + ',' + dateArr[2] + ',' + dateArr[3] + ',' + dateArr[4] + ',' + dateArr[5] + ',' + dateArr[6]);
						return new Date(dateArr[0], dateArr[1] - 1, dateArr[2], dateArr[3], dateArr[4]);
					} else {
						return new Date();
					}
				}
			},
			'description',
			'address',
			'pos_lat',
			'pos_lon',
			'pos_alt',
			'registered_at',
			'registration_no',
			'stat',
			'deadline',
			'authority_response',
			'authority_response_date',
			'user_rate',
			'user_feedback',
			'claim_classifier_id',
			'claim_classifier',
			'departament'
			
			
		]
	}
});



// "topics": [
//     "Conversation",
//     "Live Stream"
// ],
// "pretty_time": "2:00pm",
// "room": "Grand Ballroom",
// "end_time": "2010-11-16T14:25:00",
// "description": "TBD",
// "title": "A Conversation with Carol Bartz, CEO, Yahoo!",
// "url": "http://www.web2summit.com/web2010/public/schedule/detail/15362",
// "date": "11/16/2010",
// "speakers": []
// "id": 15362,
// "time": "2010-11-16T14:00:00",
// "proposal_type": "Plenary",
// "day": "Tuesday, 11/16/2010"
