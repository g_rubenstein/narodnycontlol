Ext.define('NarodnyjControl.store.Claims', {
	extend: 'Ext.data.Store',

	requires: ['Ext.DateExtras', 
		'Ext.data.proxy.Rest'],

    config: {

        model: 'NarodnyjControl.model.Claim',

        autoLoad: false,

        grouper: {
            sortProperty: 'created_at',
            groupFn: function(record) {
                return Ext.Date.format(record.get('created_at'), 'Y-m-d G:i');
            }
        },

        proxy: {
            type: 'rest',
            format: 'json'
        },

        sorters: [
            {
                property: 'created_at',
                direction: 'ASC'
            }
        ]
    }
});
