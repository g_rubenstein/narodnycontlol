Ext.define('NarodnyjControl.view.claim.Card', {

    extend: 'Ext.NavigationView',
    xtype: 'claimContainer',

    config: {

        title: 'Claims',
        iconCls: 'time',

        autoDestroy: false,
	navigationBar: {
            ui: 'sencha',
            items: [
                {
                    xtype: 'button',
                    itemId: 'newButton',
                    text: 'New',
                    align: 'right',
                    hidden: false,
                    hideAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeOut',
                        duration: 200
                    },
                    showAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeIn',
                        duration: 200
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'saveButton',
                    text: 'Save',
                    align: 'right',
                    hidden: true,
                    hideAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeOut',
                        duration: 200
                    },
                    showAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeIn',
                        duration: 200
                    }
                }
            ]
        },

        items: [
            {
                xtype: 'claims',
                store: 'Claims',
                grouped: true,
                pinHeaders: false
            }
        ]
    }
});
