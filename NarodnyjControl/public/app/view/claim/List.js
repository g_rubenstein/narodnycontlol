Ext.define('NarodnyjControl.view.claim.List', {

	extend: 'Ext.List',
	requires: 'Ext.SegmentedButton',

	xtype: 'claims',

	config: {
		items: [
			{
				docked: 'top',
				xtype: 'toolbar',
				ui: 'gray',

				items: [
					{
						width: '100%',
						defaults: {
							flex: 1
						},
						xtype: 'segmentedbutton',
						allowDepress: false
					}
				]
			}
		],
        variableHeights: true,
        useSimpleItems: true,
		itemTpl: [
			'<div class="claimColumn"><div class="title">{title}</div><div class="description">{description}</div></div>'
		]
	},

	initialize: function() {
		this.config.title = 'Обращения';
		this.callParent();

		var segmentedButton = this.down('segmentedbutton');

		//segmentedButton.add("one");
		//segmentedButton.add("two");
		//segmentedButton.add("three");
	}
});
