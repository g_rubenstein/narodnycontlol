Ext.define('NarodnyjControl.view.claim.Detail', {

	extend : 'Ext.form.Panel',
	xtype : 'claim',
	itemId : 'detailForm',
	config : {

		layout : 'vbox',
		title : '',
		items : [{
			xtype : 'textfield',
			name : 'title',
			label : 'title',
			value: 'Заголовок'
		}, {
			xtype : 'textfield',
			name : 'description',
			label : 'description',
			value: 'Описание'
		}, {
			xtype : 'textfield',
			name : 'address',
			label : 'address',
			value: 'Ростов-на-Дону'
		}, {
			xtype : 'numberfield',
			name : 'pos_lat',
			label : 'pos_lat',
			value: 23
		}, {
			xtype : 'numberfield',
			name : 'pos_lon',
			label : 'pos_lon',
			value: 24
		}, {
			xtype : 'numberfield',
			name : 'pos_alt',
			label : 'pos_alt',
			value: 25
		}, {
			xtype : 'datepickerfield',
			name : 'created_at',
			label : 'created_at',
			 value: new Date()
		}, {
			xtype : 'numberfield',
			name : 'claim_classifier_id',
			label : 'claim_classifier_id',
			value: 7
		}		
		]
	},
	initialize: function() {
		this.callParent();

		var frm = Ext.get('uploadForm')
		frm.dom.setAttribute('action', App.apiUri + 'claims');
	}
});
