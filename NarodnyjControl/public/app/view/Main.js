Ext.define('NarodnyjControl.view.Main', {

	extend: 'Ext.tab.Panel',
	xtype: 'main',

	config: {

		tabBarPosition: 'bottom',
		tabBar: {
			ui: 'gray'
		},

		items: [
			{ xclass: 'NarodnyjControl.view.claim.Card' }
		]
	}
});
