Ext.define('NarodnyjControl.view.Login', {
	extend : 'Ext.tab.Panel',
	xtype : 'main',
	requires : ['Ext.TitleBar', 'Ext.form.Panel', 'Ext.form.FieldSet', 'Ext.field.Number', 'Ext.field.Spinner', 'Ext.field.Password', 'Ext.field.Email', 'Ext.field.Url', 'Ext.field.DatePicker', 'Ext.field.Select', 'Ext.field.Hidden', 'Ext.field.Radio', 'Ext.field.Slider', 'Ext.field.Toggle', 'Ext.field.Search', 'Ext.util.JSON'],
	config : {
		tabBarPosition : 'bottom',
		fullscreen : true,
		items : [{
			title : 'Login',
			iconCls : 'home',
			xtype: 'formpanel',
			id : 'loginform',
			items : [{
				title : 'Login',
				items : [{
					xtype : 'panel',
					items : [{
						title : 'Home',
						iconCls : 'home',
						html : ['<center><img src="/resources/images/narkon.png"/></center>'].join("")
					}]
				}, {
					xtype : 'fieldset',
					title : 'Log into the system',
					instructions : 'Please enter the credentials above. <br>' + 'Click <a href="#" onclick="javascript:restorePassword(); return false;">here</a> if you lost your password.',
					defaults : {
						labelWidth : '35%'
					},
					items : [{
						xtype : 'textfield',
						name : 'login',
						label : 'Login',
						placeHolder : 'User name',
						autoCapitalize : false,
						required : true,
						clearIcon : true,
						value: 'test'
					}, {
						xtype : 'passwordfield',
						name : 'password',
						label : 'Password',
						value: '1234'
					}]
				}, {
					xtype : 'panel',
					layout : {
						type : 'hbox'
					},
					padding : '0 0 5 5',
					items : [{
						id : 'credentialsWarningPanel',
						baseCls : 'x-form-fieldset-instructions',
						html : '*Your credentials will be stored locally. You can disable this feature using application settings window.'
					}]
				}, {
					xtype : 'panel',
					layout : {
						type : 'hbox'
					},
					padding : '0 0 5 5',
					items : [{
						id : 'versionPanel',
						baseCls : 'x-form-fieldset-instructions',
						html : 'Application version: TODO'
					}]
				}, {
					xtype : 'panel',
					defaults : {
						xtype : 'button',
						style : 'margin: 0.1em',
						margin : '0 20px',
						flex : 1
					},
					layout : {
						type : 'hbox'
					},
					items : [{
						text : 'Reset',
						handler : function() {
							Ext.getCmp('loginform').reset();
						}
					}, {
						text : 'Log in',
						scope : this,
						hasDisabled : false,
						id : 'loginButton'
					}]
				}]
			}]
		}, {
			title : 'Register',
			iconCls : 'action',

			items : [{
				docked : 'top',
				xtype : 'titlebar',
				title : 'Getting Started'
			}]
		}]
	}
});
