require 'test_helper'

class ClaimClassifiersControllerTest < ActionController::TestCase
  setup do
    @claim_classifier = claim_classifiers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:claim_classifiers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create claim_classifier" do
    assert_difference('ClaimClassifier.count') do
      post :create, claim_classifier: { begin_date: @claim_classifier.begin_date, description: @claim_classifier.description, end_date: @claim_classifier.end_date, name: @claim_classifier.name }
    end

    assert_redirected_to claim_classifier_path(assigns(:claim_classifier))
  end

  test "should show claim_classifier" do
    get :show, id: @claim_classifier
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @claim_classifier
    assert_response :success
  end

  test "should update claim_classifier" do
    patch :update, id: @claim_classifier, claim_classifier: { begin_date: @claim_classifier.begin_date, description: @claim_classifier.description, end_date: @claim_classifier.end_date, name: @claim_classifier.name }
    assert_redirected_to claim_classifier_path(assigns(:claim_classifier))
  end

  test "should destroy claim_classifier" do
    assert_difference('ClaimClassifier.count', -1) do
      delete :destroy, id: @claim_classifier
    end

    assert_redirected_to claim_classifiers_path
  end
end
