require 'test_helper'

class Api::AuthControllerTest < ActionController::TestCase
  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get logout" do
    get :logout
    assert_response :success
  end

  test "should get reset" do
    get :reset
    assert_response :success
  end

  test "should get register" do
    get :register
    assert_response :success
  end

  test "should get registrationStatus" do
    get :registrationStatus
    assert_response :success
  end

end
