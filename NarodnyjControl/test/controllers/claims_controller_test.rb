require 'test_helper'

class ClaimsControllerTest < ActionController::TestCase
  setup do
    @claim = claims(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:claims)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create claim" do
    assert_difference('Claim.count') do
      post :create, claim: { address: @claim.address, authority_response: @claim.authority_response, authority_response_date: @claim.authority_response_date, category_id: @claim.category_id, created_at: @claim.created_at, creator_id: @claim.creator_id, deadline: @claim.deadline, description: @claim.description, originator_id: @claim.originator_id, pos_alt: @claim.pos_alt, pos_lat: @claim.pos_lat, pos_lon: @claim.pos_lon, rate: @claim.rate, registered_at: @claim.registered_at, registration_no: @claim.registration_no, state: @claim.state, title: @claim.title }
    end

    assert_redirected_to claim_path(assigns(:claim))
  end

  test "should show claim" do
    get :show, id: @claim
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @claim
    assert_response :success
  end

  test "should update claim" do
    patch :update, id: @claim, claim: { address: @claim.address, authority_response: @claim.authority_response, authority_response_date: @claim.authority_response_date, category_id: @claim.category_id, created_at: @claim.created_at, creator_id: @claim.creator_id, deadline: @claim.deadline, description: @claim.description, originator_id: @claim.originator_id, pos_alt: @claim.pos_alt, pos_lat: @claim.pos_lat, pos_lon: @claim.pos_lon, rate: @claim.rate, registered_at: @claim.registered_at, registration_no: @claim.registration_no, state: @claim.state, title: @claim.title }
    assert_redirected_to claim_path(assigns(:claim))
  end

  test "should destroy claim" do
    assert_difference('Claim.count', -1) do
      delete :destroy, id: @claim
    end

    assert_redirected_to claims_path
  end
end
