# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130819210000) do

  create_table "api_versions", force: true do |t|
    t.string   "version"
    t.string   "uri"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "claim_classifiers", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "begin_date"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
  end

  add_index "claim_classifiers", ["ancestry"], name: "index_claim_classifiers_on_ancestry"

  create_table "claims", force: true do |t|
    t.integer  "originator_id"
    t.string   "title"
    t.text     "description"
    t.text     "address"
    t.float    "pos_lat"
    t.float    "pos_lon"
    t.float    "pos_alt"
    t.datetime "created_at"
    t.datetime "registered_at"
    t.string   "registration_no"
    t.integer  "responsible_id"
    t.integer  "departament_id"
    t.string   "status"
    t.integer  "status_code"
    t.datetime "deadline"
    t.text     "authority_response"
    t.datetime "authority_response_date"
    t.integer  "user_rate"
    t.text     "user_feedback"
    t.datetime "updated_at"
    t.integer  "claim_classifier_id"
  end

  add_index "claims", ["claim_classifier_id"], name: "index_claims_on_claim_classifier_id"
  add_index "claims", ["created_at"], name: "index_claims_on_created_at"
  add_index "claims", ["departament_id"], name: "index_claims_on_departament_id"
  add_index "claims", ["originator_id"], name: "index_claims_on_originator_id"
  add_index "claims", ["status_code"], name: "index_claims_on_status_code"

  create_table "sessions", force: true do |t|
    t.string   "sid"
    t.string   "isid"
    t.datetime "access_time"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "login"
    t.string   "email"
    t.string   "pass_hash"
    t.string   "address"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "patronymic_name"
    t.string   "last_name"
    t.string   "settings"
  end

end
