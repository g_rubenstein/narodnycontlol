class CreateClaimClassifiers < ActiveRecord::Migration
  def change
    create_table :claim_classifiers do |t|
      t.string :name
      t.string :description
      t.datetime :begin_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
