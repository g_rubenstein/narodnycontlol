class AddSettingsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :settings, :string
  end

  def self.down
    remove_column :users, :settings
  end

end
