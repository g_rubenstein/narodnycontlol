class AddAncestryToClaimClassifier < ActiveRecord::Migration
  def change
    add_column :claim_classifiers, :ancestry, :string
    add_index :claim_classifiers, :ancestry
  end

  def self.down
    remove_index :claim_classifiers, :ancestry
    remove_column :claim_classifiers, :ancestry
  end

end
