class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :sid
      t.string :isid
      t.datetime :access_time
      t.integer :user_id

      t.timestamps
    end
  end
end
