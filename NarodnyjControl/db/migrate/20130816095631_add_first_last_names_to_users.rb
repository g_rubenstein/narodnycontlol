class AddFirstLastNamesToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :fio
    add_column :users, :first_name, :string
    add_column :users, :patronymic_name, :string
    add_column :users, :last_name, :string
  end

  def self.down
    add_column :users, :fio, :string
    remove_column :users, :first_name
    remove_column :users, :patronymic_name
    remove_column :users, :last_name
  end

end
