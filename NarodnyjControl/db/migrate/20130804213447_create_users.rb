class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :fio
      t.string :login
      t.string :email
      t.string :pass_hash
      t.string :address
      t.string :phone

      t.timestamps
    end
  end
end
