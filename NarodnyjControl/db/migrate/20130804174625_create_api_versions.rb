class CreateApiVersions < ActiveRecord::Migration
  def change
    create_table :api_versions do |t|
      t.string :version
      t.string :uri
      t.string :description

      t.timestamps
    end
  end
end
