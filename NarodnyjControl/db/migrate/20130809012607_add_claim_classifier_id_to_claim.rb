class AddClaimClassifierIdToClaim < ActiveRecord::Migration
  def change
    #add_column :claims, :claim_classifier_id, :integer
    add_reference :claims, :claim_classifier, index: true
    remove_column :claims, :category_id
  end
end
