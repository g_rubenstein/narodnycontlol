class CreateClaims < ActiveRecord::Migration
  def change
    create_table :claims do |t|
      t.integer :originator_id		# Citizen (mobile/web user)
      t.string :title			# Claim title
      t.text :description		# Claim description
      t.integer :category_id		# Claim category identifier
      t.text :address			# Free-text address, related to the current claim
      t.float :pos_lat			# GPS latitude/longitude/altitude
      t.float :pos_lon
      t.float :pos_alt
      t.datetime :created_at		# Date created. May be different that db record creation date for mobile draft claims
      t.datetime :registered_at		# Authority registration date
      t.string :registration_no		# Authority registration number
      t.integer :responsible_id		# Responsible personnel
      t.integer :departament_id		# Authority departament
      t.string :status		# Claim state
      t.integer :status_code
      t.datetime :deadline		# Deadline for this claim according to category deadline rules
      t.text :authority_response        # Response from authority
      t.datetime :authority_response_date #Date of response
      t.integer :user_rate		# User feedback rate				
      t.text :user_feedback		# User feedback text

      t.timestamps
    end
    add_index :claims, :originator_id
    add_index :claims, :departament_id
    add_index :claims, :status_code
    add_index :claims, :created_at
  end

  def self.down
    remove_index :claims, :originator_id
    remove_index :claims, :creator_id
    remove_index :claims, :departament_id
    remove_index :claims, :status_code
    remove_index :claims, :created_at
  end

end

