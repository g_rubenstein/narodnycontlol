#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Create default API version
ApiVersion.create(:version => 'v1', :uri => 'api/v1/%{SID}/', :description => 'API v1.0')
ApiVersion.create(:version => 'iosv1-0', :uri => 'api/v1/%{SID}/', :description => 'API v1.0')

user = User.create(:first_name => 'Иван', :patronymic_name => 'Иванович', :last_name => 'Петров', :login => 'ivan', :email => 'ivan@example.com', 
  :password => '123', :address => 'Ростов-на-Дону, ул Полевая 12 кв 45', :phone => '+79185436765', :settings => {:email_notifications => true, :push_notifications => false})
  
mike = User.create(:first_name => 'Михаил', :last_name => 'Шкутков', :login => 'test', :email => 'mike@example.com', 
  :password => '1234', :address => 'Ростов-на-Дону', :phone => '+123', :settings => {:email_notifications => true, :push_notifications => false})

roads = ClaimClassifier.create(:name => 'Дороги', :description => 'Состояние дорожного покрытия', :end_date => Time.now + 1.year);

homes = ClaimClassifier.create(:name => 'ЖКХ', :description => 'Состояние объектов ЖКХ', :end_date => Time.now + 1.year);

cc1 = ClaimClassifier.create(:name => 'Ямы', :description => 'Регистрация дефектов дорожного покрытия', :end_date => Time.now + 1.year, 
    :parent_id => roads.id);
ClaimClassifier.create(:name => 'Канализация', :description => 'Регистрация дефектов подземных инженерных сооружений', :end_date => Time.now + 1.year, 
    :parent_id => roads.id);
ClaimClassifier.create(:name => 'Дорожные знаки', :description => 'Состояние дорожных знаков', :end_date => Time.now + 1.year, 
    :parent_id => roads.id);

ClaimClassifier.create(:name => 'Крыши', :description => 'Обледенение крыш', :end_date => Time.now + 1.year, 
    :parent_id => homes.id);
cc2 = ClaimClassifier.create(:name => 'Мусор', :description => 'Вывоз мусора', :end_date => Time.now + 1.year, 
    :parent_id => homes.id);    
ClaimClassifier.create(:name => 'Благоустройство', :description => 'Состояние прилегающих территорий', :end_date => Time.now + 1.year, 
    :parent_id => homes.id);    
    

Claim.create(:originator_id => mike.id, :title => 'Яма', 
  :description => 'Яма на перекрестке ул Мечникова и Халтуринский',
  :address => 'Россия, Ростовская область, Ростов-на-Дону, улица Мечникова, 124/171', :status => 'sent', :status_code => 1,
  :pos_lat => 47.238308, :pos_lon => 39.694006, :pos_alt => 0.0, :created_at => Time.now, :claim_classifier_id => cc1.id)

Claim.create(:originator_id => mike.id, :title => 'Мусор', 
  :description => 'Мусор на перекрестке ул Мечникова и Халтуринский',
  :address => 'Россия, Ростовская область, Ростов-на-Дону, улица Мечникова, 124/171', :status => 'decision', :status_code => 5,
  :pos_lat => 47.238308, :pos_lon => 39.694006, :pos_alt => 0.0, :created_at => Time.now, :claim_classifier_id => cc2.id,
  :registered_at => Time.now + 1.day, :registration_no => 'ВХ №34523', :deadline => Time.now + 20.days,
  :responsible_id => 778, :departament_id => 45)


