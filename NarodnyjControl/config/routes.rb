NarodnyjControl::Application.routes.draw do
  resources :claims

  resources :claim_classifiers

  resources :users

  resources :sessions

  resources :api_versions

  #Auth main. login/logout
  post 'api/v1/auth/' => 'api/auth#login'
  delete 'api/v1/auth' => 'api/auth#logout'
  
  #Auth supplementary
  post 'api/v1/auth/resetPassword' => 'api/auth#resetPassword'
  post 'api/v1/auth/register' => 'api/auth#register'
  put 'api/v1/auth/' => 'api/auth#update'
  put 'api/v1/auth/settings' => 'api/auth#update_settings'
  

  #Classifier
  get "api/v1/:sid/classifiers/(:date)" => 'api/classifiers#get'

  #Claims
  get 'api/v1/:sid/claims/(:date)' => 'api/claims#index'
  post 'api/v1/:sid/claims/' => 'api/claims#create'
  put 'api/v1/:sid/claims/:id' => 'api/claims#update'
  patch 'api/v1/:sid/claims/:id' => 'api/claims#update'
  delete 'api/v1/:sid/claims/:id' => 'api/claims#delete'
  
  #Attachments
  get 'api/v1/:sid/attachments/:id/:number/preview' => 'api/attachments#preview'
    
  root 'default#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
