module ErrorHelper
  def render_error(action, message)
    render :json => {:error => {:action => action, :message => message}}
  end
  
  def render_error_show(message)
    render :json => {:error => {:action => 'show', :message => message}}
  end
  
  def render_empty_json
    render :json => {}
  end
  
  def render_session_error
    render_error('relogin', 'session.expired')
  end
  
  def render_no_session_login_error
    render_error('relogin', 'session.auth')
  end
  
  def valid_session?
    @session = Session.where(["sid=?", params[:sid]]).first
    
    #  Check session existense
    return false unless !@session.nil?
    
    inactiveTime = Time.now - @session.access_time
    
    # Check sessin timeout
    if SETTINGS['session_timeout_seconds'] < inactiveTime
      @session.destroy
      return false
    end
    
    # Update session last access time stamp
    @session.access_time = Time.now
    @session.save
    
    return true
    
  end
  
  def valid_user?
    return false unless valid_session?
    return @session.user_id != -1
  end
end